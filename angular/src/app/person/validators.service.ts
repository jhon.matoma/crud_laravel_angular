import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidatorsService {

  constructor() { }

  requiredFileType( types: string[] ) {
    types = types.map(value => value.toLowerCase());
    return function (control: FormControl) {
      const file = control.value;
      if ( file ) {
        const extension = file.type.substring(file.type.indexOf('/') + 1, file.type.length);
        //const extension = file.substring(file.indexOf('/') + 1, file.indexOf(';'));
        if ( !types.includes(extension.toLowerCase()) ) {
          return {
            requiredFileType: true
          };
        }
        return null;
      }
      return null;
    };
  }
}
