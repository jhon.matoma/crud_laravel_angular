import { Component, OnInit } from '@angular/core';
import { PersonService } from '../person.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

import { Person } from '../person';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  id: number;
  person: Person;
  Banner = [];
  type = [{
    type: 'Banner frontal',
  },
  {
    type: 'Banner lateral'
  },
  ];  /* {}; []*/
  loading;
  loading_file;
  success_load_file;
  error_load_file;
  formView: FormGroup;
  form: FormGroup;
  public previsualizacion:string;

  constructor(
    public personService: PersonService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuild: FormBuilder,
    private sanitizer:DomSanitizer
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['idPerson'];
    this.personService.find(this.id).subscribe((data: Person)=>{
      this.person = data;
    });

    this.form = this.formBuild.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      file: [''],
      image: ['', [Validators.required]],
    });
    this.formView = this.formBuild.group({
      uuid: [{ value: '', disabled: true }],
      payment_method: [{ value: '', disabled: true }],
      amount: [{ value: '', disabled: true }],
    });

  }

  get f(){
    return this.form.controls;
  }
  onFileChange(event, item) {
    this.success_load_file = false;
    this.error_load_file = false;
    
    if (event.target.files && event.target.files.length > 0) {

      const file = event.target.files[0];

      // 
    this.extraerBase64(file).then((imagen:any)=>{
      this.previsualizacion = imagen.base;
      console.log(imagen);
    })
    // 
      // 
      // this.previsualizacion = 
      // 
      this.form.patchValue({
        [item]: file
      });

      const form = {
        file: file
      };
    }
  }

  

  submit(){
    console.log(this.form.value);
    this.personService.update(this.form.value,this.id).subscribe(res => {
         console.log('Person updated successfully!');
         this.router.navigateByUrl('person/index');
    })
  }
  extraerBase64 = async ($event: any) => new Promise((resolve, reject) => {
    try {
      const unsafeImg = window.URL.createObjectURL($event);
      const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
      const reader = new FileReader();
      reader.readAsDataURL($event);
      reader.onload = () => {
        resolve({
          base: reader.result
        });
      };
      reader.onerror = error => {
        resolve({
          base: null
        });
      };

    } catch (e) {
      return null;
    }
  })

}
