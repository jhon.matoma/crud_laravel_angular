import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {  Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Person } from './person';
import { FormGroup, FormArray } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class VariasPersonasService {

  private apiURL = "http://localhost:8000/api/addmore/";


  httpOptions = {
     headers: new HttpHeaders({
       'Content-Type': 'application/json'
     })
  }

  constructor( 
    private httpClient: HttpClient,
    private http: HttpClient) { 

      this.headers = new HttpHeaders({
        Accept: 'application/json',
      });
    }
    private readonly headers: any;

    addMorePost(data): Observable<any> {
      const formData = new FormData();
      for (const key in data) {
        formData.append(key, data[key]);    
      }
      console.log(formData);
      return this.httpClient.post<any>(this.apiURL,formData, { headers: this.headers })
      .pipe(
        catchError(this.errorHandler)
      )
    }


    addPerson(data): Observable<any> {
      const body=JSON.stringify(data);
      console.log(body)
      return this.httpClient.post(this.apiURL ,body,{ headers: this.headers })
    }
    
    create(person): Observable<any> {
      return this.httpClient.post<any>(this.apiURL,person, { headers: this.headers })
        .pipe(
          catchError(this.errorHandler)
        )
    }

    // create(person): Observable<any> {
    //   // console.log(person);
      

    //   const formData = new FormData();

      
    //   let data=[];

    //   for (var i = 0; i < person.length; i++) {
    
    //     for (const key in person[i]) {
    //       console.log(key,person[i][key]);  

    //         formData.append(key, person[i][key]);           
    //         // console.log(formData);

    //     }     
    //     data.push(formData);   
    //     console.log(data);

    // }

    //   return this.httpClient.post<any>(this.apiURL,data, { headers: this.headers })
    //   .pipe(
    //     catchError(this.errorHandler)
    //   )
    // }

    errorHandler(error) {
      let errorMessage = '';
      if(error.error instanceof ErrorEvent) {
        errorMessage = error.error.message;
      } else {
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }
      return throwError(errorMessage);
    }
}
