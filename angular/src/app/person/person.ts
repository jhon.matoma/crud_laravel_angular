export interface Person {
    id: number;
    name: string;
    email: string;
    phone: number;
    file:string;
    image:string;
}
