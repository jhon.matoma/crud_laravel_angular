import { Component, OnInit } from '@angular/core';
import { VariasPersonasService } from '../varias-personas.service';
import { FormBuilder, FormControl, FormGroup, Validators,FormArray } from '@angular/forms';
// import { ValidatorsService } from '../validators.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-agregar-varios',
  templateUrl: './agregar-varios.component.html',
  styleUrls: ['./agregar-varios.component.css']
})
export class AgregarVariosComponent implements OnInit {
  userTable: FormGroup;
  formView: FormGroup;
  control: FormArray;
  public previsualizacion=[];
  success_load_file;
  error_load_file;
  mode:false; 

  // mode: boolean;
  touchedRows: any;
  constructor(private fb: FormBuilder,private router: Router, public variasPersonasService: VariasPersonasService,private sanitizer:DomSanitizer) { }

  ngOnInit(): void {
    this.touchedRows = [];
    this.userTable = this.fb.group({
      tableRows: this.fb.array([])
    });
    this.addRow();
  }
  
  ngAfterOnInit() {
    this.control = this.userTable.get('tableRows') as FormArray;
  }

  initiateForm(): FormGroup {
    return this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      file: [''],
      image: [''],
    });
   
  }

  addRow() {
    const control =  this.userTable.get('tableRows') as FormArray;
    control.push(this.initiateForm());
  }

  deleteRow(index: number) {
    const control =  this.userTable.get('tableRows') as FormArray;
    control.removeAt(index);
  }

 

  saveUserDetails() {
    console.log(this.userTable.value);
  }

  get getFormControls() {
    const control = this.userTable.get('tableRows') as FormArray;
    return control;
  }

  submitForm() {
    const control = this.userTable.get('tableRows') as FormArray;
    this.touchedRows = control.controls.filter(row => row.touched).map(row => row.value);
    // console.log(this.touchedRows);  

    this.variasPersonasService.create(this.touchedRows).subscribe(res => {
      console.log('Person created successfully!');
      // this.router.navigateByUrl('person/index');

 })

  }
 
  onFileChange1(event, item, i) {       
    

    const reader = new FileReader();
    let imageSrc: string;
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      // console.log(file);
      reader.readAsDataURL(file);
      reader.onload = () => {
        imageSrc = reader.result as string;
      this.previsualizacion[i] = reader.result;

      // console.log(imageSrc);

        item.patchValue({
          image: reader.result
          // image: file

        });
      };
    }
  }

  // onFileChange(event, item, index) {
  //   console.log(item);
  //   this.success_load_file = false;
  //   this.error_load_file = false;
    
  //   if (event.target.files && event.target.files.length > 0) {

  //     const file = event.target.files[0];

  //     // 
  //   this.extraerBase64(file).then((imagen:any)=>{
  //     this.previsualizacion[index] = imagen.base;
  //     // console.log(imagen.base);
  //   })
   
  //     item.patchValue({
  //       image: file
  //     });

  //     console.log(this.userTable);
      
  //     const form = {
  //       file : file
  //     };

  //   }
  // }

  extraerBase64 = async ($event: any) => new Promise((resolve, reject) => {
    try {
      const unsafeImg = window.URL.createObjectURL($event);
      const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
      const reader = new FileReader();
      reader.readAsDataURL($event);
      reader.onload = () => {
        resolve({
          base: reader.result
        });
      };
      reader.onerror = error => {
        resolve({
          base: null
        });
      };

    } catch (e) {
      return null;
    }
  })

 
}