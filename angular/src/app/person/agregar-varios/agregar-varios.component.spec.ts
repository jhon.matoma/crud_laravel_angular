import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarVariosComponent } from './agregar-varios.component';

describe('AgregarVariosComponent', () => {
  let component: AgregarVariosComponent;
  let fixture: ComponentFixture<AgregarVariosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarVariosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarVariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
