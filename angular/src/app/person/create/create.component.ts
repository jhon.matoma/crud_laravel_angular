import { Component, OnInit } from '@angular/core';
import { PersonService } from '../person.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
// import { ValidatorsService } from '../validators.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  Banner = [];
  type = [{
    type: 'Banner frontal',
  },
  {
    type: 'Banner lateral'
  },
  ];  /* {}; []*/
  loading;
  loading_file;
  success_load_file;
  error_load_file;
  formView: FormGroup;
  form: FormGroup;
  public previsualizacion:string;

  constructor(
    public personService: PersonService,
    private router: Router,
    private formBuild: FormBuilder,
    private sanitizer:DomSanitizer
    // private validatorsSrv: ValidatorsService
  ) { }

  ngOnInit(): void {

    this.form = this.formBuild.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      file: [''],
      image: [''],
    });
   
}
  

  get f(){
    return this.form.controls;
  }

  onFileChange(event, item) {
    this.success_load_file = false;
    this.error_load_file = false;
    
    if (event.target.files && event.target.files.length > 0) {

      const file = event.target.files[0];

      // 
    this.extraerBase64(file).then((imagen:any)=>{
      this.previsualizacion = imagen.base;
      console.log(imagen);
    })

    console.log(item);
    console.log(file);
   
      this.form.patchValue({
        [item]: file
      });

      const form = {
        file: file
      };
    }
  }
  submit(){
    console.log(this.form.value);
    this.personService.postdata(this.form.value).subscribe(res => {
         console.log('Person created successfully!');
        //  this.router.navigateByUrl('person/index');
    })
  }
  extraerBase64 = async ($event: any) => new Promise((resolve, reject) => {
    try {
      const unsafeImg = window.URL.createObjectURL($event);
      const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
      const reader = new FileReader();
      reader.readAsDataURL($event);
      reader.onload = () => {
        resolve({
          base: reader.result
        });
      };
      reader.onerror = error => {
        resolve({
          base: null
        });
      };

    } catch (e) {
      return null;
    }
  })

}
