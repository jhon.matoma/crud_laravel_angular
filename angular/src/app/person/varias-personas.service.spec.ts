import { TestBed } from '@angular/core/testing';

import { VariasPersonasService } from './varias-personas.service';

describe('VariasPersonasService', () => {
  let service: VariasPersonasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VariasPersonasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
