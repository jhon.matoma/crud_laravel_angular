<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\PersonController;
use App\Http\Controllers\ProductAddMoreController;
use App\Http\Controllers\FormController;




/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('person')->group(function () {
    Route::get('/',[ PersonController::class, 'getAll']);
    Route::post('/',[ PersonController::class, 'create']);
    Route::delete('/{id}',[ PersonController::class, 'delete']);
    Route::get('/{id}',[ PersonController::class, 'get']);
    Route::put('/{id}',[ PersonController::class, 'update']);
}); 

// Route::prefix('addmore')->group(function () {
//     Route::get('/',[ ProductAddMoreController::class, 'addMore']);
//     Route::post('/',[ ProductAddMoreController::class, 'addMorePost']);
// }); 
Route::get("addmore","App\Http\Controllers\ProductAddMoreController@addMore");
Route::post("addmore","App\Http\Controllers\ProductAddMoreController@addMorePost")->name('addmorePost');

Route::get("/","App\Http\Controllers\FormController@index");
Route::post("upload_data","App\Http\Controllers\FormController@store");
