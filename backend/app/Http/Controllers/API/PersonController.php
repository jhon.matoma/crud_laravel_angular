<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use App\Models\Person;
use Illuminate\Support\Facades\File;

class PersonController extends Controller
{
    // https://carbon.now.sh/
    public function getAll(){
      $data = Person::get();
      return response()->json($data, 200);
    }

    public function create(Request $request){
  
      $person = new Person();
      $url_image = $this->upload($request->file('image'));
      $person->name= $request->input('name');
      $person->email = $request->input('email');
      $person->phone= $request->input('phone');
      $person->image= $url_image;
      $res = $person->save();

      if ($res) {
        return response()->json(['message' => 'Post create succesfully'], 201);
    }
    return response()->json(['message' => 'Error to create post'], 500);
     

    
    }


    // public function update( Request $request, $id)
  
    // }

    private function upload($image)
    {
        $path_info = pathinfo($image->getClientOriginalName());
        $post_path = 'images/post';

        $rename = uniqid() . '.' . $path_info['extension'];
        $image->move(public_path() . "/$post_path", $rename);
        return "$post_path/$rename";
    }

    private function subirArchivo($file)
    {
        $nombreArchivo = time(). '.'. $file->getClientOriginalExtension();
        $file->move(public_path('images/post'), $nombreArchivo);
        return $nombreArchivo;
    }

    public function delete($id){
      $res = Person::find($id)->delete();
      return response()->json([
          'message' => "Successfully deleted",
          'success' => true
      ], 200);
    }

    public function get($id){
      $data = Person::find($id);
      return response()->json($data, 200);
    }

  //PUT para modificar datos
  public function update(Request $request, $id)
  {
    $person = Person::find($id);
    $person->name = $request->input('name');
    $person->email = $request->input('email');
    $person->phone = $request->input('phone');
    if ($request->hasfile('image'))
    {
      $destination = public_path('images/post/'.$person->image);
      if (File::exists($destination)) {
          File::delete($destination);
      }
      $file=$request->file('image');
      $extention=$file->getClientOriginalExtension();
      $filename=time().'.'.$extention;
      $file->move('images/post/',$filename);
      $person->image=$filename;

    }
    $person->update();
    // return redirect()->back()->with('status','Persona actualizada satisfatoriamente ');

  }


  // public function update(Request $request, Person $bannerData)
  // {
  //   $bannerId = $request->id;
  //   $bannerData = Person::find($bannerId);
  //   if ($request->hasFile('image')){
  //     $image_path = public_path("/uploads/resource/".$bannerData->banner_name);
  //     if (File::exists($image_path)) {
  //         File::delete($image_path);
  //     }
  //     $bannerImage = $request->file('image');
  //     $imgName = $bannerImage->getClientOriginalName();
  //     $destinationPath = public_path('/uploads/resource/');
  //     $bannerImage->move($destinationPath, $imgName);
  //   } else {
  //     $imgName = $bannerData->banner_name;
  //   }
  
  //   $bannerData->name = $request->name;
  //   $bannerData->email = $request->email;
  //   $bannerData->phone = $request->phone;

  //   $bannerData->image = $imgName;
  //   $bannerData->save();
  // }

    }
    

    

