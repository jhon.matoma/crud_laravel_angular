<?php

namespace App\Http\Controllers;

use App\Models\Person;
use Illuminate\Http\Request;

class FormController extends Controller
{
    public function store(Request $request){
            $this->validate($request,[
                'image' =>'required'
                
            ]);
            if ($request->hasfile('image')){
                foreach($request->file('image') as $image)
                {
                    $name=$image->getClientOriginalName();
                    $image->move(public_path().'/image/',$name);
                    $data=$name;
                }
            }
            $upload_model=new Person;
            $upload_model->image =$data;
            $upload_model->save();

            return response()->json(['message' => 'exito'], 200);
                }
}
