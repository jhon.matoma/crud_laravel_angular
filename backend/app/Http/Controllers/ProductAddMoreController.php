<?php
   
namespace App\Http\Controllers;
    
use Illuminate\Http\Request;
Use App\Models\Person;
// Use intervention\image\src\Intervention\Image\Facades\Image;
use Intervention\Image\Facades\Image as Image;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
// use Intervention\Image\Facades\Image;

// use Intervention\Image\Facades\Image;
// use Intervention\Image\Image;
// use Intervention\Image\ImageManagerStatic as Image;


   
class ProductAddMoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addMore()
    {
        return view("addMore");
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addMorePost(Request $request)
    {
        if($request->isMethod('post'))
        {
            $userData = $request->all();
            // echo "pre"; print_r($userData);die;

            foreach ( $userData as $key => $value){

                $person = new Person();
                // $url_image = $this->upload($value['image']);
                $person->name= $value['name'];
                $person->email = $value['email'];
                $person->phone= $value['phone'];
                $person->image= $value['image'];

                $image= $person->image;
                preg_match("/data:image\/(.*?);/",$image,$image_extension); // extract the image extension
                $image = preg_replace('/data:image\/(.*?);base64,/','',$image); // remove the type part
                $image = str_replace(' ', '+', $image);
                $imageName = 'image_' . time() . $key.'.' . 'jpg' ; //generating unique file name;
                Storage::disk('public')->put($imageName,base64_decode($image));
                

                $person->image= "storage/$imageName";
                $person->save();

            // echo "pre"; print_r($res);die;


            }
            
            return response()->json(['message'=>'Usuarios agregados satisfatoriamente']);
        }

        // return response()->json(json_encode($request->all()));
        // $person = new Person();

        // $this->validate($request,[
        //     '*.name' => 'required',
        //     '*.email' => 'required',
        //     '*.phone' => 'required',
        //     // '*.image' => 'required',

        // ]); 

        // $image = $request->input('*.image'); // image base64 encoded
        // // echo "pre"; print_r($image);die;


        // preg_match("/data:image\/(.*?);/",$image,$image_extension); // extract the image extension
        // $image = preg_replace('/data:image\/(.*?);base64,/','',$image); // remove the type part
        // $image = str_replace(' ', '+', $image);
        // $imageName = 'image_' . time() . '.' . 'jpg' ; //generating unique file name;
        // Storage::disk('public')->put($imageName,base64_decode($image));
        // $request->$image=$imageName;
        // echo "pre"; print_r($imageName);die;


        // echo "pre"; print_r($request->all());die;
                 

        //  dd($request->all());
        // foreach ($request->all() as $key => $value) {
        //     Person::create($value);
        // }
    
        // if ($value) {
            // return response()->json(['message' => 'Post create succesfully'], 201);
        // }
        // return response()->json(['message' => 'Error to create post'], 500);
    }

    private function upload($image)
    {
        $path_info = pathinfo($image->getClientOriginalName());
        $post_path = 'images/post';

        $rename = uniqid() . '.' . $path_info['extension'];
        $image->move(public_path() . "/$post_path", $rename);
        return "$post_path/$rename";
    }
}